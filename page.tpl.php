<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">

  <head>
    <title><?php print $head_title ?></title>
    <meta http-equiv="Content-Style-Type" content="text/css" />
	<?php print $head ?>
	<?php print $styles ?>
	<?php print $scripts ?>
	<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
  </head>

<body>
  <div id="main">

   <?php if (count($primary_links)) : ?>
     <div id="topmenu">
     <?php print theme('links', $primary_links); ?>
     </div>
   <?php endif; ?>
   <?php if (count($secondary_links)) : ?>
     <div id="topmenu-secondary">
     <?php print theme('links', $secondary_links); ?>
     </div>
   <?php endif; ?>

    <?php if ($site_name) : ?>
        <div id="header">
          <span class="head-title"><?php print(substr($site_name, 0, 1)) ?><span class="head-darktitle"><?php print(substr($site_name,1,15)) ?></span></span><br/>
        </div>
    <?php endif;?>

    <div id="lefty">
      <?php if ($left) { ?>
        <?php print $left ?>
      <?php } ?>
      <?php if ($right != ""): ?>
        <?php print $right ?> <!-- only print right sidebar if any blocks enabled -->
      <?php endif; ?> 
  
    </div>

    <div id="content">
      <div id="breadcrumb">
       <?php print $breadcrumb ?>
      </div>

      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <?php print $help ?>
      <?php print $messages ?>

      <h1 class="title"><?php print $title ?></h1>
      <div class="tabs"><?php print $tabs ?></div>

      <?php print $content; ?>

      <div id="footer">
        <?php print $footer_message ?>
      </div>
      <?php print $closure ?>

    </div>

</div>


</body>
</html>

